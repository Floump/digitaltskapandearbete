﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Draw
{
    public partial class Form1 : Form
    {
        bool init = true;
        bool painting = false;
        int size = 5;

        Graphics graphics;
        Point lastPoint;
        Pen pen;

        public Form1()
        {
            InitializeComponent();

            tsmi_Size.Text = size.ToString();

            graphics = canvas.CreateGraphics();

            pen = new Pen(Color.Black, size);
            pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
            pen.EndCap = pen.StartCap;

            init = false;
        }

        private void canvas_MouseDown(object sender, MouseEventArgs e)
        {
            painting = true;

            lastPoint = e.Location;
            graphics.FillEllipse(pen.Brush, new Rectangle(e.X - size / 2, e.Y - size / 2, size, size));
        }

        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (painting)
            {
                graphics.DrawLine(pen, lastPoint, e.Location);
                lastPoint = e.Location;
            }
        }

        private void canvas_MouseUp(object sender, MouseEventArgs e)
        {
            painting = false;
        }

        private void tsmi_Size_TextChanged(object sender, EventArgs e)
        {
            if (!init)
            {
                if (int.TryParse(tsmi_Size.Text, out size))
                {
                    pen.Width = size;
                }
                else { tsmi_Size.Text = size.ToString(); }
            }
        }

        private void tsmi_New_Click(object sender, EventArgs e)
        {
            DialogResult result = SaveMessage(MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes) { tsmi_Save_Click(new object(), new EventArgs()); }
            canvas.Invalidate();
        }

        private void tsmi_Save_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PNG Image|*.png|JPEG Image|*.jpg";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Bitmap image = new Bitmap(canvas.Width, canvas.Height);
                canvas.DrawToBitmap(image, canvas.ClientRectangle);
                image.Save(sfd.FileName);
            }
        }

        private void tsmi_Color_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();

            if (cd.ShowDialog() == DialogResult.OK) { pen.Color = cd.Color; }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = SaveMessage(MessageBoxButtons.YesNoCancel);

            if (result == DialogResult.Yes) { tsmi_Save_Click(new object(), new EventArgs()); }
            else if (result == DialogResult.Cancel) { e.Cancel = true; }
        }

        DialogResult SaveMessage(MessageBoxButtons buttons)
        {
            return MessageBox.Show(
                "Do you want to save your current work before creating a new one?\nData may be lost",
                "User Message", buttons, MessageBoxIcon.Information);
        }
    }
}
